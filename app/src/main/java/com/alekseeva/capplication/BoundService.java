package com.alekseeva.capplication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import com.alekseeva.aidl.CameraData;
import com.alekseeva.aidl.ICameraDataInterface;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by Julia Alekseeva on 05.04.2021.
 */

public class BoundService extends Service {

  static {
    System.loadLibrary("native-lib");
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return new ICameraDataInterface.Stub() {
      @Override
      public List<CameraData> getCamerasCharacteristics() throws RemoteException {
        return Arrays.asList(getCameraCharacteristics());
      }
    };
  }

  public native CameraData[] getCameraCharacteristics();

}

