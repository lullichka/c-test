package com.alekseeva.capplication;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.hardware.camera2.CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY;

public class MainActivity extends AppCompatActivity {

  private final int CAMERA_REQUEST_CODE = 1112;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    TextView tv = findViewById(R.id.sample_text);
    tv.setText(R.string.this_is_app);
    if (!isNativeCamSupported()) {
      Toast.makeText(this, "Native camera probably won't work on this device!",
          Toast.LENGTH_LONG).show();
      finish();
    }
  }

  private boolean isNativeCamSupported() {
    CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
    boolean nativeCamSupported = false;
    try {
      for (String camId : camManager.getCameraIdList()) {
        CameraCharacteristics characteristics = camManager.getCameraCharacteristics(camId);
        int hwLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        if (hwLevel != INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
          nativeCamSupported = true;
          break;
        }
      }
    } catch (CameraAccessException e) {
      e.printStackTrace();
    }
    return nativeCamSupported;
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (ContextCompat.checkSelfPermission(this,
        Manifest.permission.CAMERA)
        != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat
          .requestPermissions(
              MainActivity.this,
              new String[]{Manifest.permission.CAMERA},
              CAMERA_REQUEST_CODE);
    }
  }

  public void onRequestPermissionsResult(int requestCode,
                                         @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    super
        .onRequestPermissionsResult(requestCode,
            permissions,
            grantResults);

    if (requestCode == CAMERA_REQUEST_CODE) {
      // Checking whether user granted the permission or not.
      if (grantResults.length > 0
          && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        // Showing the toast message
        Toast.makeText(MainActivity.this,
            "Camera Permission Granted",
            Toast.LENGTH_SHORT)
            .show();
      } else {
        Toast.makeText(MainActivity.this,
            "Camera Permission Denied",
            Toast.LENGTH_SHORT)
            .show();
      }
    }
  }
}