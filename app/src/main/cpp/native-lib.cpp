#include <jni.h>
#include <string>
#include <list>
#include <camera/NdkCameraManager.h>
#include "log.h"
#include <media/NdkImageReader.h>
#include <memory>

typedef struct _JNI_POSREC {
    jclass cls;
    jmethodID constructorID;
    jfieldID dataID;
} JNI_POSREC;

struct SearchRecord {
    std::string id;
    std::string facing;
};
JNI_POSREC *jniPosRec = NULL;

void LoadJniPosRec(JNIEnv *env) {
    if (jniPosRec != NULL)
        return;
    jniPosRec = new JNI_POSREC;
    jniPosRec->cls = env->FindClass("com/alekseeva/aidl/CameraData");
    if (jniPosRec->cls != NULL)
        LOGD("sucessfully created class");
    jniPosRec->constructorID = env->GetMethodID(jniPosRec->cls, "<init>", "()V");
    if (jniPosRec->constructorID != NULL) {
        LOGD("sucessfully created ctorID");
    }
    jniPosRec->dataID = env->GetFieldID(jniPosRec->cls, "data", "Ljava/util/HashMap;");
}

SearchRecord getCamProps(const char *string, ACameraManager *cameraManager);


extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_alekseeva_capplication_BoundService_getCameraCharacteristics(JNIEnv *env, jobject thiz) {
    ACameraManager *cameraManager = ACameraManager_create();
    auto uptr1  = std::unique_ptr<ACameraManager, std::function<void(ACameraManager *)>>(cameraManager,
                                                                                        [](ACameraManager *manager) {
                                                                                            ACameraManager_delete(manager);
                                                                                        });
    ACameraIdList *cameraIds = nullptr;
    ACameraManager_getCameraIdList(uptr1.get(), &cameraIds);
    auto uptr = std::unique_ptr<ACameraIdList, std::function<void(ACameraIdList *)>>(cameraIds,
                                                                                     [](ACameraIdList *list) {
                                                                                         ACameraManager_deleteCameraIdList(
                                                                                                 list);
                                                                                     });
    jniPosRec = nullptr;
    LoadJniPosRec(env);
    jobjectArray jPosRecArray = env->NewObjectArray(uptr->numCameras, jniPosRec->cls, nullptr);
    for (size_t i = 0; i < uptr->numCameras; i++) {
        SearchRecord pRecord1 = getCamProps(uptr->cameraIds[i], cameraManager);
        jobject jPosRec = env->NewObject(jniPosRec->cls, jniPosRec->constructorID);
        jclass mapClass = env->FindClass("java/util/HashMap");
        if (mapClass == nullptr) {
            return nullptr;
        }
        jsize map_len = uptr->numCameras;
        jmethodID init = env->GetMethodID(mapClass, "<init>", "(I)V");
        jobject hashMap = env->NewObject(mapClass, init, map_len);
        jmethodID put = env->GetMethodID(mapClass, "put",
                                         "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
        env->CallObjectMethod(hashMap, put, env->NewStringUTF("id"),
                              env->NewStringUTF(pRecord1.id.c_str()));
        env->CallObjectMethod(hashMap, put, env->NewStringUTF("facing"),
                              env->NewStringUTF(pRecord1.facing.c_str()));
        env->SetObjectField(jPosRec, jniPosRec->dataID, hashMap);
        env->SetObjectArrayElement(jPosRecArray, i, jPosRec);
    }

    return jPosRecArray;

}

SearchRecord getCamProps(const char *id, ACameraManager *cameraManager) {
    SearchRecord *pRecord1 = new SearchRecord();
    pRecord1->id = id;
    ACameraMetadata *metadataObj;
    ACameraManager_getCameraCharacteristics(cameraManager, id, &metadataObj);
    ACameraMetadata_const_entry entry = {0};
    // cam facing
    ACameraMetadata_getConstEntry(metadataObj,
                                  ACAMERA_SENSOR_ORIENTATION, &entry);

    int32_t orientation = entry.data.i32[0];
    LOGD("camProps: %d", orientation);
    pRecord1->facing = std::to_string(orientation);
    // JPEG format
    ACameraMetadata_getConstEntry(metadataObj,
                                  ACAMERA_SCALER_AVAILABLE_STREAM_CONFIGURATIONS, &entry);

    for (int i = 0; i < entry.count; i += 4) {
        // We are only interested in output streams, so skip input stream
        int32_t input = entry.data.i32[i + 3];
        if (input)
            continue;

        int32_t format = entry.data.i32[i + 0];
        if (format == AIMAGE_FORMAT_JPEG) {
            int32_t width = entry.data.i32[i + 1];
            int32_t height = entry.data.i32[i + 2];
            LOGD("camProps: maxWidth=%d vs maxHeight=%d", width, height);
        }
    }
    // sensitivity
    ACameraMetadata_getConstEntry(metadataObj,
                                  ACAMERA_SENSOR_INFO_SENSITIVITY_RANGE, &entry);

    int32_t minSensitivity = entry.data.i32[0];
    int32_t maxSensitivity = entry.data.i32[1];

    LOGD("camProps: minSensitivity=%d vs maxSensitivity=%d", minSensitivity, maxSensitivity);

    ACameraMetadata_getConstEntry(metadataObj,
                                  ACAMERA_SENSOR_INFO_EXPOSURE_TIME_RANGE, &entry);

    int64_t minExposure = entry.data.i64[0];
    int64_t maxExposure = entry.data.i64[1];
    LOGD("camProps: minExposure=%lld vs maxExposure=%lld", minExposure, maxExposure);
    return *pRecord1;
}

