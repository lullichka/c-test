// ICameraDataInterface.aidl
package com.alekseeva.aidl;

import com.alekseeva.aidl.CameraData;

interface ICameraDataInterface {

     List<CameraData> getCamerasCharacteristics();
}