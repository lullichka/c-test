package com.alekseeva.aidl;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by Julia Alekseeva on 05.04.2021.
 */

public final class CameraData implements Parcelable {

  public CameraData() {
  }

  private HashMap<String, String> data = new HashMap<>();

  public HashMap<String, String> getData() {
    return data;
  }

  public void setData(HashMap<String, String> data) {
    this.data = data;
  }

  public CameraData(HashMap<String, String> data) {
    this.data = data;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeMap(data);
  }

  public static final Creator<CameraData> CREATOR = new Parcelable.Creator<CameraData>() {
    public CameraData createFromParcel(Parcel in) {
      return new CameraData(in);
    }

    public CameraData[] newArray(int size) {
      return new CameraData[size];
    }
  };

  private CameraData(Parcel in) {
    in.readMap(data, String.class.getClassLoader());
  }
}
