package com.alekseeva.client

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alekseeva.aidl.ICameraDataInterface
import com.alekseeva.client.util.observeCommandSafety
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var viewmodel:MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProvider(this).get(MainViewModel::class.java)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupRv()
        btn_start.setOnClickListener {
            viewmodel.startDoing()
        }
        btn_stop.setOnClickListener {
            viewmodel.stopDoing()
        }
        btn_start_1.setOnClickListener {
            viewmodel.startDoingWithExecutor()
        }
        btn_stop_1.setOnClickListener {
            viewmodel.stopDoingWithExecutor()
        }
    }

    private fun setupViewModel(){
        viewmodel.run {
            observeCommandSafety(viewState.camerasInfo){
                (rv_data.adapter as CameraDataAdapter).setItems(it)
            }
        }
    }

    private fun setupRv() {
        rv_data.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_data.adapter = CameraDataAdapter()
    }

    private var cameraData: ICameraDataInterface? = null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            cameraData = ICameraDataInterface.Stub.asInterface(service)
            viewmodel.setData(cameraData)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            cameraData = null
        }
    }


    override fun onStart() {
        super.onStart()
        bindService(createExplicitIntent(), serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unbindService(serviceConnection)
    }
}

private fun Activity.createExplicitIntent(): Intent {
    val intent = Intent("com.example.aidl.REMOTE_CONNECTION")
    val services = packageManager.queryIntentServices(intent, 0)
    if (services.isEmpty()) {
        throw IllegalStateException("Приложение-сервер не установлено")
    }
    return Intent(intent).apply {
        val resolveInfo = services[0]
        val packageName = resolveInfo.serviceInfo.packageName
        val className = resolveInfo.serviceInfo.name
        component = ComponentName(packageName, className)
    }
}