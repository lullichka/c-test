package com.alekseeva.client.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/**
 * Created by Julia Alekseeva on 12.04.2021.
 */

fun <T> LifecycleOwner.observeCommand(data: LiveData<T>, action: (T?) -> Unit) {
    data.observe(this, Observer(action))
}

fun <T> LifecycleOwner.observeCommandSafety(data: LiveData<T>, action: (T) -> Unit) {
    observeCommand(data) {
        it?.also(action)
    }
}