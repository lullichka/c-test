package com.alekseeva.client

import androidx.lifecycle.ViewModel
import com.alekseeva.aidl.ICameraDataInterface
import com.alekseeva.client.state.MainViewState
import com.alekseeva.client.state.MainViewStateImpl
import com.alekseeva.lib.SomeConsumer
import com.alekseeva.lib.SomeProducer
import java.util.concurrent.*

/**
 * Created by Julia Alekseeva on 12.04.2021.
 */
class MainViewModel : ViewModel() {
    private val mRunningTaskList: ArrayList<Future<String>> = ArrayList()
    var BOUND = 2
    var PRODUCERS = 3
    var CONSUMERS = 2
    var queue: BlockingQueue<String> = ArrayBlockingQueue(BOUND)
    var listProducingTasks: ArrayList<Thread> = ArrayList()
    var listConsumeringTasks: ArrayList<Thread> = ArrayList()
    var executorService: ExecutorService? = null

    private val _viewState = MainViewStateImpl()

    val viewState: MainViewState
        get() = _viewState


    fun setData(cameraData: ICameraDataInterface?) {
        cameraData?.let {
            _viewState.camerasInfo.value = it.camerasCharacteristics
        }
    }

    fun startDoing() {
        for (i in 0 until PRODUCERS) {
            val r = Thread(SomeProducer(queue))
            listProducingTasks.add(r)
            r.start()
        }
        for (i in 0 until CONSUMERS) {
            val r1 = Thread(SomeConsumer(queue))
            listConsumeringTasks.add(r1)
            r1.start()
        }
    }

    fun stopDoing() {
        for (task in listProducingTasks) {
            task.interrupt()
        }
        for (task in listConsumeringTasks) {
            task.interrupt()
        }
    }

    fun startDoingWithExecutor() {
        executorService = Executors.newCachedThreadPool()
        for (i in 0 until PRODUCERS) {
            executorService?.submit(SomeProducer(queue))
            val future: Future<String>? = executorService?.submit(SomeProducer(queue)) as Future<String>?
            future?.let { mRunningTaskList.add(it) }
        }
        for (i in 0 until CONSUMERS) {
            val future: Future<String>? = executorService?.submit(SomeConsumer(queue)) as Future<String>?
            future?.let { mRunningTaskList.add(it) }
        }
    }

    fun stopDoingWithExecutor() {
        synchronized(this) {
            queue.clear()
            for (task in mRunningTaskList) {
                if (!task.isDone) {
                    task.cancel(true)
                }
            }
            mRunningTaskList.clear()
        }
    }
}