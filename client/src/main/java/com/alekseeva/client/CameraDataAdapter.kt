package com.alekseeva.client

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alekseeva.aidl.CameraData
import kotlinx.android.synthetic.main.list_data_item.view.*

/**
 * Created by Julia Alekseeva on 05.04.2021.
 */
class CameraDataAdapter : RecyclerView.Adapter<CameraDataAdapter.CameraDataViewHolder>() {

    private val items = mutableListOf<CameraData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CameraDataViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_data_item, parent, false)
        return CameraDataViewHolder(
                view
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CameraDataViewHolder, position: Int) {
        holder.bind(
                items[position]
        )
    }

    fun setItems(items: List<CameraData>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    class CameraDataViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(
                data: CameraData
        ) {
            view.run {
                textview_id.text = "${data.data["id"]}"
                textview_orientation.text = "${data.data["facing"]}"
            }
        }
    }
}

