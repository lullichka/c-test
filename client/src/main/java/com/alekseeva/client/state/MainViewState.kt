package com.alekseeva.client.state

import androidx.lifecycle.MutableLiveData
import com.alekseeva.aidl.CameraData

/**
 * Created by Julia Alekseeva on 12.04.2021.
 */
interface MainViewState {
    val camerasInfo: MutableLiveData<List<CameraData>>
}