package com.alekseeva.lib;

/**
 * Created by Julia Alekseeva on 16.04.2021.
 */

class OneMoreGame implements Runnable {
  public final Object Lock1;
  public final Object Lock2;


  public OneMoreGame(Object resource1, Object resource2) {
    Lock1 = resource1;
    Lock2 = resource2;
  }

  @Override
  public void run() {
    synchronized (Lock2) {
      System.out.println("DeadThreadTwo is holding LOCK 2...");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println("DeadThreadOne is waiting for Lock 1...");
      synchronized (Lock1) {
        System.out.println("DeadThreadOne  is holding Lock 1 and Lock 2...");
      }
    }
  }
}
