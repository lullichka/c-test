package com.alekseeva.lib;

import java.lang.management.ThreadMXBean;
import java.util.concurrent.BlockingQueue;

import sun.rmi.runtime.Log;


/**
 * Created by Julia Alekseeva on 16.04.2021.
 */

public class SomeConsumer extends Thread {
    private final BlockingQueue<String> queue;

    public SomeConsumer(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1000);
                String message = queue.take();
                System.out.println("Consumed " + message + " in " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                System.out.println("Cancelling with exception " + Thread.currentThread().getName());
                return;
            }
        }
    }
}

