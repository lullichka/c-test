package com.alekseeva.lib;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Julia Alekseeva on 16.04.2021.
 */

public class SomeProducer extends Thread {

    private final BlockingQueue<String> queue;

    public SomeProducer(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                process();
            } catch (InterruptedException e) {
                System.out.println("Cancelling with exception " + Thread.currentThread().getName());
                return;
            }
        }
    }

    private void process() throws InterruptedException {
        queue.put(Thread.currentThread().getName());
        System.out.println("Running Producer: " + Thread.currentThread().getName());
    }
}
